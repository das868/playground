#include <unistd.h>
#include <assert.h>
#include <stdio.h>
#include <stdlib.h> //why?
#include <stdbool.h>

#define DIM_X 32 //2 bigger than needed -> margin!
#define DIM_Y 32 //2 bigger than needed -> margin!
#define ROW_NUM DIM_Y
#define COL_NUM DIM_X


typedef struct {
  //dimensions will be as above #defined
  char contents[DIM_X][DIM_Y];
} board;


void makeEmptyBoard(char (*array)[DIM_Y])
{
  for(int i=0; i<DIM_X; i++) {
    for(int j=0; j<DIM_Y; j++) {
      array[i][j] = ' ';
    }
  }
}


void markForbiddenAreas(char (*array)[DIM_Y])
{
  for(int x=0; x<DIM_X; x++) {
    array[x][0] = '.';
    array[x][DIM_Y - 1] = '.';
  }

  for(int y=0; y<DIM_Y; y++) {
    array[0][y] = '.';
    array[DIM_X - 1][y] = '.';
  }
}


void drawBoardArray(char (*array)[DIM_Y])
{
  printf("\n");
  for(int i=0; i<DIM_X; i++) {
    //printf("|");
    for(int j=0; j<DIM_Y; j++) {
      printf("%c", array[i][j]);
    }
    // printf("|\n");
    printf("\n");
  }
}


char nextState(char (*board)[DIM_Y], int x, int y)
{
  // get cell under investigation
  char self = board[x][y];
  // get all the neighbouring cells
  // starting top left clockwise
  char neighbours[] = {
    board[x-1][y-1],
    board[x][y-1],
    board[x+1][y-1],

    board[x+1][y],

    board[x+1][y+1],
    board[x][y+1],
    board[x-1][y+1],

    board[x-1][y]
  };

  //DEBUG
  /*printf("\nNeigbours: |");
  for(int i=0; i<8; i++) {
    printf("%c ", neighbours[i]);
  }
  printf("|\n");*/

  // count alive cells
  int aliveNeighbours = 0;
  for(int i=0; i<8; i++) {
    if(neighbours[i] == '*') {
      aliveNeighbours++;
    }
  }

  //DEBUG
  //printf("Alive neighbours: %d\n", aliveNeighbours);

  if(self == '*' && (aliveNeighbours == 2 || aliveNeighbours == 3)) {
    return '*';
  }
  else if(self == ' ' && aliveNeighbours == 3) {
    return '*';
  }
  else {
    return ' ';
  }
}


void nextIteration(char (*array)[DIM_Y])
{
  char result[DIM_X][DIM_Y];
  for(int i=0; i<DIM_X; i++) {
    for(int j=0; j<DIM_Y; j++) {
      result[i][j] = array[i][j];
    }
  }

  for(int i=1; i<DIM_X-1; i++) {
    for(int j=1; j<DIM_Y-1; j++) {
      result[i][j] = nextState(array, i, j);
    }
  }
  array = result;
}




int main(void)
{
  /////////////////////////////////////////////////////////
  //no edge/margin case support; everything must be inside!
  /////////////////////////////////////////////////////////
  
  board theboard;
  makeEmptyBoard(theboard.contents);
  //drawBoardArray(theboard.contents);
  markForbiddenAreas(theboard.contents);
  theboard.contents[2][1] = '*';
  theboard.contents[2][2] = '*';
  theboard.contents[1][2] = '*';

  theboard.contents[6][5] = '*';
  theboard.contents[6][6] = '*';
  theboard.contents[6][7] = '*';
  drawBoardArray(theboard.contents);
  
  //printf("\n-------------------------\n");
  printf("\nnext State 1 1: |%c|", nextState(theboard.contents, 1, 1));
  printf("\nnext State 6 5: |%c|", nextState(theboard.contents, 6, 5));
  printf("\nnext State 6 6: |%c|", nextState(theboard.contents, 6, 6));

  sleep(1);
  printf("\n");
  system("tput clear");

  //clearScrollback();
  drawBoardArray(theboard.contents);

  // ------------------- copy into method! -------------
  //nextIteration(theboard.contents);
  for(int a=0; a<10; a++) {
    printf("\n==========\nIteration No. %d\n\n", a+1);
    char result[DIM_X][DIM_Y];
  
    for(int i=0; i<DIM_X; i++) {
      for(int j=0; j<DIM_Y; j++) {
	result[i][j] = theboard.contents[i][j];
      }
    }

    for(int i=1; i<DIM_X-1; i++) {
      for(int j=1; j<DIM_Y-1; j++) {
	result[i][j] = nextState(theboard.contents, i, j);
      }
    }
    //array = result;
    drawBoardArray(result);
  }
  // ---------------------- end copy -------------------

  //drawBoardArray(theboard.contents);
  sleep(1);
  printf("\n");
  system("tput clear");
  return 0;
}
