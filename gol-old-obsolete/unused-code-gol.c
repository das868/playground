#include <assert.h>
#include <stdio.h>
#include <stdlib.h> //why?
#include <stdbool.h>

#define ROW_NUM 20
#define COL_NUM 20


void drawBoard(void)
{
  for(int i=0; i<ROW_NUM; i++) {
    for(int j=0; j<COL_NUM; j++) {
      printf("o ");
    }
    printf("\n");
  }
}

void drawBoardArray(char (*array)[ROW_NUM])
{
  //assert(false && "not implemented...");
  for(int i=0; i<ROW_NUM; i++) {
    for(int j=0; j<COL_NUM; j++) {
      printf("%c", array[i][j]);
    }
    printf("\n");
  }
}



int main(void)
{
  drawBoard();

  char board[COL_NUM][ROW_NUM];

  for(int i=0; i<ROW_NUM; i++) {
    for(int j=0; j<COL_NUM; j++) {
      board[i][j] = '*';
    }
  }
  
  drawBoardArray(board);
  return 0;
}






// from nextState...
if(x == 0) {
    if(y == 0) {
      neighbours[0] = ' ';
      neighbours[1] = ' ';
      neighbours[2] = ' ';
      neighbours[6] = ' ';
      neighbours[7] = ' ';
    }
    else {
      neighbours[0] = ' ';
      neighbours[6] = ' ';
      neighbours[7] = ' ';
    }
  else {
    if(y == 0) {
      neighbours[0] = ' ';
      neighbours[1] = ' ';
      neighbours[2] = ' ';
    }
  }



char nextStateOLD(char (*board)[DIM_Y], int x, int y)
{
  // get all the neighbouring cells
  // starting top left clockwise
  /*char neighbours[] = {
    board[x-1][y-1],
    board[x][y-1],
    board[x+1][y-1],

    board[x+1][y],

    board[x+1][y+1],
    board[x][y+1],
    board[x-1][y+1],

    board[x-1][y]
  };*/
  char neighbours[8];

  

  
  // support margins too by making dead a default
  // lower index value margins
  switch(x)
  {
    case 0:
      if(y == 0) {
	neighbours[0] = ' ';
	neighbours[1] = ' ';
	neighbours[2] = ' ';
	neighbours[6] = ' ';
	neighbours[7] = ' ';
      }
      else if(y == DIM_Y) {
	neighbours[0] = ' ';
	neighbours[4] = ' ';
	neighbours[5] = ' ';
	neighbours[6] = ' ';
	neighbours[7] = ' ';
      }
      else {
	neighbours[0] = ' ';
	neighbours[6] = ' ';
	neighbours[7] = ' ';
      }
      break;
    case DIM_X:
      if(y == 0) {
	neighbours[0] = ' ';
	neighbours[1] = ' ';
	neighbours[2] = ' ';
	neighbours[3] = ' ';
	neighbours[4] = ' ';
      }
      else if(y == DIM_Y) {
	neighbours[2] = ' ';
	neighbours[3] = ' ';
	neighbours[4] = ' ';
	neighbours[5] = ' ';
	neighbours[6] = ' ';
      }
      else {
	neighbours[2] = ' ';
	neighbours[3] = ' ';
	neighbours[4] = ' ';
      }
      break;
    default:
      if(y == 0) {
	neighbours[0] = ' ';
	neighbours[1] = ' ';
	neighbours[2] = ' ';
      }
      else if(y == DIM_Y) {
	neighbours[4] = ' ';
	neighbours[5] = ' ';
	neighbours[6] = ' ';
      }
      break;
  }

  //DEBUG
  printf("\nNeigbours: |");
  for(int i=0; i<8; i++) {
    printf("%c ", neighbours[i]);
  }
  printf("|\n");

  // count alive cells
  int aliveNeighbours = 0;
  for(int i=0; i<8; i++) {
    if(neighbours[i] == '*') {
      aliveNeighbours++;
    }
  }

  printf("Alive neighbours: %d\n", aliveNeighbours);
  
  /*if(dead) {
    return ' ';
  }
  else if(alive) {
    return '*';
  }
  else {
    return 'x';
    }*/
  return '*';
}


void clearScrollback(void)
{
  // insert 44 newlines
  //printf("\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n");
  //printf("\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n");

  //11 lines too much for windows terminal...
  printf("\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n");
  printf("\n\n\n\n\n\n\n\n\n\n");
}
