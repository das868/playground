#include <stdio.h>
#include <stdlib.h>

//take zoom -1 for *Shell Command Output* frame!
#define LIMX 40 //40
#define LIMY 60 //20

#define DEAD '.'
#define ALIVE 'o'
#define ERROR '*'


typedef struct Board {
  char contents[LIMX][LIMY];
} Board;


void initializeBoard(Board* board, char defaultchar)
{
  for(int y=0; y<LIMY; y++) {
    for(int x=0; x<LIMX; x++) {
      board->contents[x][y] = defaultchar;
    }
  }
}


void printBoard(Board* board)
{
  printf("\n\n");
  for(int i=0; i<LIMX; i++) {
    printf("=");
  }
  printf("\n\n\n");
  //printf("\n===========================\n\n");
  
  for(int y=0; y<LIMY; y++) {
    for(int x=0; x<LIMX; x++) {
      printf("%c ", board->contents[x][y]);
    }
    printf("\n");
  }
}


char getNextState(Board* board, char state, int x, int y)
{
  //if at margins, always return DEAD; else check
  if(x == 0 || x == LIMX-1 || y == 0 || y == LIMY-1) {
    return ERROR;
  }
  else {
    //start top left and go clockwise
    char neighbours[8] = {
      board->contents[x-1][y-1],
      board->contents[x  ][y-1],
      board->contents[x+1][y-1],
      
      board->contents[x+1][y  ],
      
      board->contents[x+1][y+1],
      board->contents[x  ][y+1],
      board->contents[x-1][y+1],
      
      board->contents[x-1][y  ]
    };

    int alivecount = 0;

    for(int i=0; i<8; i++) {
      if(neighbours[i] == ALIVE) {
	alivecount++;
      }
    }

    if(state == ALIVE && (alivecount == 2 || alivecount == 3)) {
      return ALIVE;
    }
    else if(state == DEAD && alivecount == 3) {
      return ALIVE;
    }
    else if(state == ALIVE) {
      return DEAD;
    }
    else if(state == DEAD) {
      return DEAD;
    }
    else {
      return ERROR;
    }
  }
}

/* 
Board* getNextBoard(Board* board)
{
  //printf("GET NEXT BOARD INIT:\n");
  Board nextBoard;
  //Board* nextptr = &nextBoard;
  initializeBoard(&nextBoard, DEAD);
  //printBoard(&nextBoard);
  
  for(int y=0; y<LIMY; y++) {
    for(int x=0; x<LIMX; x++) {
      nextBoard.contents[x][y] = getNextState(board, board->contents[x][y], x, y);
    }
  }
  
  //printf("GET NEXT BOARD END:\n");
  //printBoard(&nextBoard);
  
  Board* nextptr = &nextBoard;
  return nextptr;
}
*/

//------------------------------------------
Board getNextBoard2(Board* board)
{
  //printf("GET NEXT BOARD INIT:\n");
  Board nextBoard;
  //Board* nextptr = &nextBoard;
  initializeBoard(&nextBoard, DEAD);
  //printBoard(&nextBoard);
  
  for(int y=0; y<LIMY; y++) {
    for(int x=0; x<LIMX; x++) {
      nextBoard.contents[x][y] = getNextState(board, board->contents[x][y], x, y);
    }
  }

  return nextBoard;
}
//------------------------------------------

 
int main()
{
  
  Board b;
  initializeBoard(&b, DEAD);

  // BOARD: LIMX=40, LIMY=20/++
  /**/
  // margin test
  b.contents[1][1] = DEAD;
  b.contents[1][2] = ALIVE;
  b.contents[2][1] = ALIVE;
  /**/
  
  /**/
  // blinker
  b.contents[4][5] = ALIVE;
  b.contents[4][6] = ALIVE;
  b.contents[4][7] = ALIVE;
  /**/

  /**/
  // toad
  b.contents[23][5] = ALIVE;
  b.contents[24][5] = ALIVE;
  b.contents[25][5] = ALIVE;
  b.contents[22][6] = ALIVE;
  b.contents[23][6] = ALIVE;
  b.contents[24][6] = ALIVE;
  /**/

  /**/
  // Pulsar!
  // Start (10,20)
  b.contents[10][22] = ALIVE;
  b.contents[10][23] = ALIVE;
  b.contents[10][24] = ALIVE;
  b.contents[10][28] = ALIVE;
  b.contents[10][29] = ALIVE;
  b.contents[10][30] = ALIVE;
  b.contents[12][20] = ALIVE;
  b.contents[12][25] = ALIVE;
  b.contents[12][27] = ALIVE;
  b.contents[12][32] = ALIVE;
  b.contents[13][20] = ALIVE;
  b.contents[13][25] = ALIVE;
  b.contents[13][27] = ALIVE;
  b.contents[13][32] = ALIVE;
  b.contents[14][20] = ALIVE;
  b.contents[14][25] = ALIVE;
  b.contents[14][27] = ALIVE;
  b.contents[14][32] = ALIVE;
  b.contents[15][22] = ALIVE;
  b.contents[15][23] = ALIVE;
  b.contents[15][24] = ALIVE;
  b.contents[15][28] = ALIVE;
  b.contents[15][29] = ALIVE;
  b.contents[15][30] = ALIVE;
  b.contents[17][22] = ALIVE;
  b.contents[17][23] = ALIVE;
  b.contents[17][24] = ALIVE;
  b.contents[17][28] = ALIVE;
  b.contents[17][29] = ALIVE;
  b.contents[17][30] = ALIVE;
  b.contents[18][20] = ALIVE;
  b.contents[18][25] = ALIVE;
  b.contents[18][27] = ALIVE;
  b.contents[18][32] = ALIVE;
  b.contents[19][20] = ALIVE;
  b.contents[19][25] = ALIVE;
  b.contents[19][27] = ALIVE;
  b.contents[19][32] = ALIVE;
  b.contents[20][20] = ALIVE;
  b.contents[20][25] = ALIVE;
  b.contents[20][27] = ALIVE;
  b.contents[20][32] = ALIVE;
  b.contents[22][22] = ALIVE;
  b.contents[22][23] = ALIVE;
  b.contents[22][24] = ALIVE;
  b.contents[22][28] = ALIVE;
  b.contents[22][29] = ALIVE;
  b.contents[22][30] = ALIVE;
  /**/

  /**/
  // glider
  b.contents[10][10] = ALIVE;
  b.contents[11][11] = ALIVE;
  b.contents[12][11] = ALIVE;
  b.contents[12][10] = ALIVE;
  b.contents[12][ 9] = ALIVE;
  /**/


  printBoard(&b);

  for(int i=0; i<10; i++){
    b = getNextBoard2(&b);
    printBoard(&b);
  }
  
  return 0;
}

// list of figures:
// https://en.wikipedia.org/wiki/Conway's_Game_of_Life#Examples_of_patterns
